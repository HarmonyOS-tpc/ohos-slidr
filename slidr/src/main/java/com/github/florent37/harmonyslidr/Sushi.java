package com.github.florent37.harmonyslidr;

import ohos.agp.components.AttrSet;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

public class Sushi extends StackLayout {
    public Sushi(Context context) {
        super(context);
    }

    public Sushi(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public Sushi(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }
}
